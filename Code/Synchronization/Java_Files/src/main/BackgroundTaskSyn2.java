/*
 * Assignment 1 - Part 2
 * Synchronized Display implementation with threads 
 * 
 * This will run the background task as a thread
 * 
 */
package src.main;

import java.util.concurrent.locks.ReentrantLock;
import src.main.OutputSwitcher;
import src.main.DisplayManager;

public class BackgroundTaskSyn2 implements Runnable{
    private DisplayManager DisplayManager;
    private String message;
    private boolean first;  //display always first
    OutputSwitcher outSwitch;
    ReentrantLock opLock;

    public BackgroundTaskSyn2(boolean first, OutputSwitcher outSwitch, DisplayManager DisplayManager, String message, ReentrantLock opLock){
        this.DisplayManager = DisplayManager;
        this.message = message;
        this.first = first;
        this.outSwitch = outSwitch;
        this.opLock = opLock;
    }

    @Override
    public void run(){
        if(first){
            for (int i=0;i<10;i++){
                while(outSwitch.switchOut){}
                opLock.lock();
                DisplayManager.display(message);
                outSwitch.changeSwitch();
                opLock.unlock();
            }
        }else{
            for (int i=0;i<10;i++){
                while(!outSwitch.switchOut){}
                opLock.lock();
                DisplayManager.display(message);
                outSwitch.changeSwitch();
                opLock.unlock();
            }
        }
    }
}