/*
 * Assignment 1 - Part 2
 * Synchronized Display implementation with threads 
 */
package src.main;

import src.main.BackgroundTaskSyn1;

public class Syn1 {

    public static void main(String args[]){
        DisplayManager messenger = new DisplayManager();
        BackgroundTaskSyn1 helloThread = new BackgroundTaskSyn1(messenger, "Hello world\n");
        BackgroundTaskSyn1 bonjourThread = new BackgroundTaskSyn1(messenger, "Bonjour monde\n");

        (new Thread(helloThread)).start();
        (new Thread(bonjourThread)).start();
    }

}