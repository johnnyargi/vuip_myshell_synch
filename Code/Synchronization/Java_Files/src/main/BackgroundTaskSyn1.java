/*
 * Assignment 1 - Part 2
 * Synchronized Display implementation with threads 
 * 
 * This will run the background task as a thread
 * 
 */
package src.main;

public class BackgroundTaskSyn1 implements Runnable{
    private DisplayManager DisplayManager;
    private String message;
    
    public BackgroundTaskSyn1(DisplayManager DisplayManager, String message){
        this.DisplayManager = DisplayManager;
        this.message = message;
    }

    @Override
    public void run(){
        for (int i=0;i<10;i++){
            DisplayManager.display(message);
        }
    }
}