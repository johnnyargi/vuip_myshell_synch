/*
 * Assignment 1 - Part 2
 * Synchronized Display implementation with threads 
 * 
 * This will be used to display the message after some time intervals
 */
package src.main;

public class DisplayManager{
    public DisplayManager(){

    }
    public synchronized void display(String message){
        for(int i=0; i<message.length(); i++){
            try{
                System.out.format("%s",message.charAt(i));
                Thread.sleep(1);
            }catch(InterruptedException e){
                
            }
        }
    }

}