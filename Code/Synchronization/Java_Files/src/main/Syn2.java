/*
 * Assignment 1 - Part 2
 * Synchronized Display implementation with threads 
 */
package src.main;

import java.util.concurrent.locks.ReentrantLock;
import src.main.BackgroundTaskSyn2;
import src.main.OutputSwitcher;

public class Syn2 {

    public static void main(String args[]){
        ReentrantLock opLock = new ReentrantLock();
        OutputSwitcher outSwitch = new OutputSwitcher(false);
        DisplayManager messenger = new DisplayManager();
        BackgroundTaskSyn2 abThread = new BackgroundTaskSyn2(true, outSwitch, messenger, "ab", opLock);
        BackgroundTaskSyn2 cdThread = new BackgroundTaskSyn2(false, outSwitch, messenger, "cd\n", opLock);

        (new Thread(abThread)).start();
        (new Thread(cdThread)).start();
    }

}