/*
 * Assignment 1 - Part 2
 * Synchronized Display implementation with threads 
 * 
 * This will be used in the "abcd" excercise to decide whether to switch from "ab" to "cd" and vice-versa
 */
package src.main;

public class OutputSwitcher{
    public volatile boolean switchOut;

    public OutputSwitcher(boolean initial){
        this.switchOut = initial;
    }

    public void changeSwitch(){
        if(switchOut){
            switchOut = false;
        }else{
            switchOut = true;
        }
    }
}