#Run this to execute the java solutions of assignment 1
#Pass an argument: "1" runs the Syn1 solution and "2" runs the Syn2 solution
if [ $1 == 1 ] 
then
    java -cp build src.main.Syn1
elif [ $1 == 2 ] 
then
    java -cp build src.main.Syn2
fi
