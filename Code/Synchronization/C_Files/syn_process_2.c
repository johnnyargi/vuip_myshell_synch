/*
* Synchronization of processes
* Assignment 1 - Part 2
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdbool.h>
#include "display.h"

int main(){
    struct sembuf up = {0, 1, 0};
    struct sembuf down = {0, -1, 0};
    int lockDisplay;    //This will protect the display function from race conditions
    int sharedMemId;    //This will be used as shared memory for storing the following variable
    bool * changeOutput;    //This will be used to indicate whether the parent or the child should print their message. Shared by both processes
    int i;
    
    //Initialization
    lockDisplay = semget(IPC_PRIVATE, 1, 0600);
    sharedMemId = shmget(IPC_PRIVATE, sizeof(bool), 0600);
    changeOutput = (bool * ) shmat(sharedMemId, 0 , 0); //attach it to this process
    semop(lockDisplay, &up, 1);
    *changeOutput = false;

    //Process spawning and usage of display
    if (fork()){
        for (i=0;i<10;i++){
            while(*changeOutput){}
            semop(lockDisplay, &down, 1);
            display("ab");
            *changeOutput = true;
            semop(lockDisplay, &up, 1);
        }
    }
    else{
        for (i=0;i<10;i++){
            while(!(*changeOutput)){}
            semop(lockDisplay, &down, 1);
            display("cd\n");
            *changeOutput = false;
            semop(lockDisplay, &up, 1);
        }
    }

    wait(NULL);
    //Detach and destroy semaphores and shared memory
    shmdt((void *) changeOutput);
    shmctl(sharedMemId, IPC_RMID, 0);
    semctl(lockDisplay, 0, IPC_RMID);
    return 0;
}