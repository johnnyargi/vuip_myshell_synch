/*
* Synchronization of processes
* Assignment 1 - Part 2
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "display.h"

int main()
{
    struct sembuf up = {0, 1, 0};
    struct sembuf down = {0, -1, 0};
    int semaphore;
    int i;
    
    //A semaphore will be used to block access to display when another process is still using it
    semaphore = semget(IPC_PRIVATE, 1, 0600);
    semop(semaphore, &up, 1);
    
    if (fork()){
        for (i=0;i<10;i++){
            semop(semaphore, &down, 1);
            display("Hello world\n");
            semop(semaphore, &up, 1);
        }
    }
    else{
        for (i=0;i<10;i++){
            semop(semaphore, &down, 1);
            display("Bonjour monde\n");
            semop(semaphore, &up, 1);
        }
    }
    wait(NULL);
    semctl(semaphore, 0, IPC_RMID);

    return 0;
}