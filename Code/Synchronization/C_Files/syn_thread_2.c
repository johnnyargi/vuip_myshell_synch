#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include "display.h"

pthread_mutex_t mutex;
bool changeOutput = false;

//Function that the spawned thread will execute
void * printMessage(){
    int i=0;
    for (i=0;i<10;i++){
        while(!changeOutput){}
        pthread_mutex_lock(&mutex);
        display("cd\n");
        changeOutput = false;
        pthread_mutex_unlock(&mutex);
    }
    return NULL;
}

//Here the main thread will be used as one of the threads 
int main(){
    int i;
    
    //Mutex creation ***************
    pthread_mutexattr_t mutAttr;
    pthread_mutexattr_init(&mutAttr);
    pthread_mutex_init(&mutex, &mutAttr);

    //Thread creation *****************
    pthread_t id;
    pthread_attr_t thrAttr;
    pthread_attr_init(&thrAttr);
    pthread_create(&id, &thrAttr, printMessage, NULL);

    //Run *****************************
    for (i=0;i<10;i++){
        while(changeOutput){}
        pthread_mutex_lock(&mutex);
        display("ab");
        changeOutput = true;
        pthread_mutex_unlock(&mutex);
    }
    //And wait for thread to finish
    pthread_join(id, NULL);
    pthread_mutex_destroy(&mutex);

    return 0;
}