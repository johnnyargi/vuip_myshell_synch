/*
* This file contains declarations regarding the following functionality : 
* 
* Parse a string input that contains commands for program execution with arguments and pipes.
* The data derived from the parsing stage should be stored for use by other programs
* The parser should identify the commands, their arguments and any pipes, that
* forward output to the next command.
*/
#ifndef COM_UTILS
#define COM_UTILS
/*
Two parsing stages are used for parsing text to multiple commands : 
    1) Separate each command by pipes
        Example : 
            ls -l|grep "text" 
            Splits to   ls -l   and     grep "text".
    2) Split each previous stage command to program and arguments - Finally create individual commands for program execution
        Example : From previous stage     ls -l   needs to be parsed as ls (the program name) and -l (the arguments)

So we use 2 lists for the first 2 stages
*/
//Restrictions on data size *******
#define MAX_CHARS 4096
#define MAX_ARGS 30
//Structs to keep data =========================================================
//Used to maintain a list of commands that the parser has detected
typedef struct command_t{
    char * program;   //the program to execute
    char * args[MAX_ARGS];     //the arguments to the command - list of strings
    struct command_t * next;    //will be used to keep a list of commands, so we can clear the memory later
}Command;
//Struct to keep a part of a parsed text. Used to create a list of parsed tokens from a parser function
typedef struct token_t{
    char text[MAX_CHARS];
    struct token_t * next;
}TokenText;
//A struct to used to build a list o pipes
typedef struct pipeNode_t{
    int fd[2];
    struct pipeNode_t * next;
}PipeNode;
//Text Parsers =================================================================
//Stage 1
//Parses a text and splits commands and pipes using "|" as dilimeter
TokenText * parseDetectPipes(char * text);
//Stage 3
//Parse a command from text. Split to program name and arguments
Command * parseCommand(char * text);
//List Utilities ===============================================================
//Add a new token to a token list
void addToken(char * text);
void addCommand(Command * cmd);
PipeNode * addNewPipe();    //add new pipe and return a pointer to it
//Debugging utilities ==========================================================
void printCommand(Command * command);
//Memory Management - Deleting Lists & Memory Cleanup ==========================
void cleanupTokenStage1List();
void cleanupCommandList();
void cleanupPipesList();
#endif