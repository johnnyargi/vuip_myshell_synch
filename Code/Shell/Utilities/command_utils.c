/*
* This file implements the functionality defined in command_utils.h
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "command_utils.h"

/*
Two parsing stages are used for parsing text to multiple commands : 
    1) Separate each command by pipes
        Example : 
            ls -l|grep "text" 
            Splits to   ls -l   and     grep "text".
    2) Split each previous stage command to program and arguments - Finally create individual commands for program execution
        Example : From previous stage     ls -l   needs to be parsed as ls (the program name) and -l (the arguments)

So we use 2 lists for the first 2 stages
*/
//Globals ==================================================================================
TokenText * tokStage1ListHead = NULL;  //The head of the list. This should be used to traverse the list of tokens
TokenText * tokStage1ListTail = NULL;  //The tail (last node) of the list. Used for faster new node addition
PipeNode * pipeListHead = NULL; //Head of the list of pipes
PipeNode * pipeListTail = NULL;
Command * commandListHead = NULL; //Head of the list of commands
Command * commandListTail = NULL;

//Text Parsers =================================================================
//Parsing Stage 1
//Parses a text and splits commands and pipes using "|" as dilimeter
TokenText * parseDetectPipes(char * text){
    char * textCopy;
    char * parsedText;
    //to avoid strtok changing the original text, we need a copy of it
    textCopy = (char *) malloc(strlen(text)*(sizeof(char)));  
    strcpy(textCopy, text);
    parsedText = strtok(textCopy, "|");
    while(parsedText != NULL){
        addToken(parsedText);
        parsedText = strtok(NULL, "|");
    }
    return tokStage1ListHead;
}
//Parse a command from text. Split to program name and arguments
Command * parseCommand(char * text){
    int i;
    char * parsedText;
    char * textCopy;
    Command * newCmd;
    //to avoid strtok changing the original text, we need a copy of it
    textCopy = (char *) malloc(strlen(text)*(sizeof(char)));  
    strcpy(textCopy, text);
    //Allocate memory for the various parts of the command and pass the data parsed
    newCmd = (Command *) malloc(sizeof(Command));
    parsedText = strtok(textCopy, " \n");
    newCmd->program = (char *) malloc(strlen(parsedText)*(sizeof(char)));
    strcpy(newCmd->program, parsedText);
    //The first argument must be the prorgam name
    newCmd->args[0] = (char *) malloc(strlen(newCmd->program)*(sizeof(char)));
    strcpy(newCmd->args[0], newCmd->program);
    //Then keep parsing for arguments
    parsedText = strtok(NULL, " \n");
    i = 1;
    while((parsedText != NULL) && (i < MAX_ARGS)){
        newCmd->args[i] = (char *) malloc(strlen(parsedText)*(sizeof(char)));
        strcpy(newCmd->args[i], parsedText);
        i++;
        parsedText = strtok(NULL, " \n");
    }
    //NULL terminate the list for execvp to detect end
    if( i < MAX_ARGS){
        newCmd->args[i] = NULL;
    }else{
        newCmd->args[i-1] = NULL;
    }
    newCmd->next = NULL;
    addCommand(newCmd);
    return newCmd;
}
//List Utilities ==============================================================
//Add a new token to a token list
void addToken(char * text){
    //create new token
    TokenText * newTok;
    newTok = (TokenText *) malloc(sizeof(TokenText));
    strcpy(newTok->text, text);
    newTok->next = NULL;
    //insert to list
    if(tokStage1ListHead == NULL){   //empty
        tokStage1ListHead = newTok;
        tokStage1ListTail = tokStage1ListHead;
    }else if(tokStage1ListHead == tokStage1ListTail){   //one element only
        tokStage1ListHead->next = newTok;
        tokStage1ListTail = newTok;
    }else{  //put in the end of the list
        tokStage1ListTail->next = newTok;
        tokStage1ListTail = newTok;
    }
}
//Add a command to the list
void addCommand(Command * cmd){
    //insert to list
    if(commandListHead == NULL){   //empty
        commandListHead = cmd;
        commandListTail = commandListHead;
    }else if(commandListHead == commandListTail){   //one element only
        commandListHead->next = cmd;
        commandListTail = cmd;
    }else{  //put in the end of the list
        commandListTail->next = cmd;
        commandListTail = cmd;
    }
}
//Add a new token to a token list
PipeNode * addNewPipe(){
    PipeNode * newPipe;
    newPipe = (PipeNode *) malloc(sizeof(PipeNode));
    pipe(newPipe->fd);
    newPipe->next = NULL;
    //insert to list
    if(pipeListHead == NULL){   //empty
        pipeListHead = newPipe;
        pipeListTail = pipeListHead;
    }else if(pipeListHead == pipeListTail){   //one element only
        pipeListHead->next = newPipe;
        pipeListTail = newPipe;
    }else{  //put in the end of the list
        pipeListTail->next = newPipe;
        pipeListTail = newPipe;
    }
    return newPipe;
}
//Memory Management - Deleting Lists & Memory Cleanup ==============================================================
void cleanupTokenStage1List(){
    TokenText * currPtr;
    TokenText * prevPtr;
    currPtr = prevPtr = tokStage1ListHead;
    while(currPtr != NULL){
        currPtr = currPtr->next;
        free(prevPtr);
        prevPtr = currPtr;
    }
    tokStage1ListHead = NULL;
    tokStage1ListTail = NULL;
}
void cleanupCommandList(){
    Command * currPtr;
    Command * prevPtr;
    currPtr = prevPtr = commandListHead;
    while(currPtr != NULL){
        currPtr = currPtr->next;
        free(prevPtr);
        prevPtr = currPtr;
    }
    commandListHead = NULL;
    commandListTail = NULL;
}
void cleanupPipesList(){
    PipeNode * currPtr;
    PipeNode * prevPtr;
    currPtr = prevPtr = pipeListHead;
    while(currPtr != NULL){
        currPtr = currPtr->next;
        close(prevPtr->fd[0]);
        close(prevPtr->fd[1]);
        free(prevPtr);
        prevPtr = currPtr;
    }
    pipeListHead = NULL;
    pipeListTail = NULL;
}
//Debugging Utils ======================================
void printCommand(Command * command){
    int i;
    printf("[Debug] Prorgam : %s\n",command->program);
    for(i=0; command->args[i] != NULL; i++){
        printf("[Debug] Argument %d : %s\n", i, command->args[i]);
    }
}