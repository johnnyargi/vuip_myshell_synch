/*
* This file is used for testing
* the functionality of various parts of this application
*/
#include <stdio.h>
#include <stdlib.h>
#include "../Utilities/command_utils.h"

int main(int argc, char * argv[]){
    char text[23]="ls -l;mkdir \"john\";pwd";
    char text2[35] = "ls -l|grep \"text\";mkdir \"john\";pwd";
    TokenText * tokens;
    TokenText * tokens2;
    Command * cmd;
    int i;

    printf("Testing com utils ......\n");
    //Checking the parsers
    //******************
    printf("\n");
    printf("Case : Semicolon Parser Output ======= \n");
    printf("Passing text : %s\n", text);
    cleanupTokenStage1List();
    tokens = parseDetectCommandEnd(text);
    while(tokens != NULL){
        printf("%s\n",tokens->text);
        tokens = tokens->next;
    }
    //******************
    printf("\n");
    printf("Case : Pipes Parser Output ======= \n");
    printf("Passing text : %s\n", text2);
    cleanupTokenStage2List();
    tokens = parseDetectPipes(text2);
    while(tokens != NULL){
        printf("%s\n",tokens->text);
        tokens = tokens->next;
    }
    //******************
    printf("\n");
    printf("Case : First Semocilon and output to Pipes Parser ======= \n");
    printf("Passing text : %s\n", text2);
    cleanupTokenStage1List();
    cleanupTokenStage2List();
    tokens = parseDetectCommandEnd(text2);
    while(tokens != NULL){
        printf("Stage 1 text : %s\n",tokens->text);
        //Parse this command further and print the new commands separately
        tokens2 = parseDetectPipes(tokens->text);
        while(tokens2 != NULL){ 
            printf("    Stage 2 : %s\n",tokens2->text);
            tokens2 = tokens2->next;
        }
        cleanupTokenStage2List(); //we used up all the stage 2 tokens
        tokens = tokens->next;
    }
    //***************************
    printf("\n");
    printf("Case : All 3 stages ======= \n");
    printf("Passing text : %s\n", text2);
    cleanupTokenStage1List();
    cleanupTokenStage2List();
    tokens = parseDetectCommandEnd(text2);
    while(tokens != NULL){
        printf("Stage 1 text : %s\n",tokens->text);
        //Parse this command further and print the new commands separately
        tokens2 = parseDetectPipes(tokens->text);
        while(tokens2 != NULL){ 
            printf("    Stage 2 : %s\n",tokens2->text);
            cmd = parseCommand(tokens2->text);
            printf("        Stage 3 - program : %s\n", cmd->program);
            if(cmd->args != NULL){
                for(i=1; cmd->args[i]!= NULL; i++){
                    printf("        Stage 3 - arguments : %s\n", cmd->args[i]);
                }
            }
            tokens2 = tokens2->next;
            //free(cmd);
        }
        cleanupTokenStage2List(); //we used up all the stage 2 tokens
        tokens = tokens->next;
    }

    //Cleanup Memory
    cleanupTokenStage1List();
    cleanupTokenStage2List();
    return 0;
}
