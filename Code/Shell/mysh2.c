/*
* This program will emulate the behavior of a shell.
* It will accept commands from user input and execute 
* them in another process.
*
* This is version 2 : command arguments supported
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include "Utilities/command_utils.h"

#define MAX_INPUT 256

int main(int argc, char * argv[]){
    pid_t pid;
    char input[MAX_INPUT];
    bool exitShell = false;
    Command * cmd;  //the command that is formed after parsing

    //Keep checking for input and handle it
    while(!exitShell){
        printf("$ ");
        fgets(input, MAX_INPUT, stdin);
        cmd = parseCommand(input);
        if(cmd->program != NULL){
            //Handle exit and cd before forking
            if(strcmp(cmd->program, "exit") == 0){  //handle exit calls
                exitShell = true;
            }
            else if(strcmp(cmd->program, "cd") == 0){    //handle cd commamd
                chdir(cmd->args[1]);    //first argument contains the program name, so we get the second for the path
            }else{
                //spawn new process
                pid = fork();
                if(pid < 0){
                    //perror("Error in forking");
                }else{
                    if(pid == 0){ //child
                        execvp(cmd->program,cmd->args); //now switch to new code
                        //perror("Error in switching child code");
                        exit(1);
                    }else{ //parent
                        wait(NULL);
                    }
                }
            }
        }
        free(cmd);
    }
    return 0;
}