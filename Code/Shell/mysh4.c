/*
* This program will emulate the behavior of a shell.
* It will accept commands from user input and execute 
* them in another process.
*
* This is version 4 : arguments supported, multiple pipes supported 
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include "Utilities/command_utils.h"
//Globals =========================================================
#define MAX_INPUT 4096

bool exitShell = false;
TokenText * tokensStage1;   //tokens after Stage 1 parsing: separate commands by "|"
Command * command;  //the command that is formed after parsing
PipeNode * inputPipe; //the child will use it to read input from it
PipeNode * outputPipe; //the child will use it to write output to it
char readBuffer[MAX_INPUT];  //the father will read last child's output from this buffer, when using pipes. That way it will have to block until the last child has produced an output

//Reset data *************************************
//Reset only pipe related data
void resetPipes(){
    cleanupPipesList();
    outputPipe = NULL;
    inputPipe = NULL;
}
//Cleanup Everything allocated
void cleanupMemory(){
    cleanupPipesList();
    cleanupCommandList();
    cleanupTokenStage1List();   
}
//Hanlde commands ************************************************************************
//Handle a command wether using pipes or not
void handleCommand(Command * command , bool usePipes, bool readInputFromPipe){
    pid_t pid;

    if(command->program != NULL){
        if(strcmp(command->program, "exit") == 0){  //handle exit calls
            exitShell = true;
        }
        else if(strcmp(command->program, "cd") == 0){    //handle cd commamd
            chdir(command->args[1]);    //first argument contains the program name, so we need the second for the path
        }else{
            //spawn new process
            pid = fork();
            if(pid == 0){ //child
                if(usePipes){
                    if(readInputFromPipe){
                        dup2(inputPipe->fd[0], 0); //wire your input to this pipe 
                        close(inputPipe->fd[0]);
                        close(inputPipe->fd[1]);
                    }
                }
                dup2(outputPipe->fd[1], 1); //wire the output to this pipe
                close(outputPipe->fd[0]);
                close(outputPipe->fd[1]); 
                execvp(command->program,command->args); //now switch to new code
                exit(1);
            }
        }
    }
}
//Parent - execute a common command - no pipes
void executeSimpleCommand(){
    command = parseCommand(tokensStage1->text);
    outputPipe = addNewPipe();
    handleCommand(command, false, false);       
    memset(readBuffer,'\0', MAX_INPUT); //we need to clear the buffer otherwise we will write on top of the previous output next time
    close(outputPipe->fd[1]);
    while(read(outputPipe->fd[0], readBuffer, MAX_INPUT) > 0){};    //read all data that the last child outputed
    close(outputPipe->fd[0]);
    printf("%s", readBuffer);
    while(waitpid(-1, NULL, WNOHANG) >0){} //wait for all children 
}
//Handle pipes - This is for the parent
void handlePipes(){
    int childrenCount;

    childrenCount = 1;
    do{ 
        command = parseCommand(tokensStage1->text);
        inputPipe = outputPipe;
        outputPipe = addNewPipe();
        if(childrenCount == 1){
            handleCommand(command, true, false);
        }
        else{
            handleCommand(command, true, true);
        }
        if(inputPipe != NULL){//exits from previous stage
            close(inputPipe->fd[0]);
            close(inputPipe->fd[1]);
        }            
        childrenCount ++;
        tokensStage1 = tokensStage1->next;
    }while(tokensStage1 != NULL);
    memset(readBuffer,'\0', MAX_INPUT); //we need to clear the buffer otherwise we will write on top of the previous output next time
    close(outputPipe->fd[1]);
    while(read(outputPipe->fd[0], readBuffer, MAX_INPUT) > 0){};    //read all data that the last child outputed
    close(outputPipe->fd[0]);
    printf("%s", readBuffer);
    while(waitpid(-1, NULL, WNOHANG) >0){} //wait for all children
    resetPipes();
}
/*
* Main program **************************************************************
*/
int main(int argc, char * argv[]){
    char input[MAX_INPUT];  //buffer to capture input from stdin

    //Run Shell loop
    while(!exitShell){
        printf("$ ");
        fgets(input, MAX_INPUT, stdin);
        //Parse command tokens
        if(strcmp(input, "\n") != 0){
            //Parse command to detect program name and arguments
            //Also handle the case of using pipes
            tokensStage1 = parseDetectPipes(input);
            if(tokensStage1->next == NULL){ //no pipes detected
                executeSimpleCommand();
            }else{ //pipes were detected
                handlePipes();
            }
            cleanupMemory();
        }
    }
    return 0;
}
